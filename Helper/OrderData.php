<?php

namespace Tone\Integration\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\SalesRule\Model\Coupon;
use Magento\SalesRule\Model\Rule;

class OrderData extends AbstractHelper
{
    public function __construct(
        CollectionFactory $orderCollectionFactory,
        Coupon $coupon,
        Rule $saleRule
    ) {
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_coupon = $coupon;
        $this->_saleRule = $saleRule;
    }

    public function getCustomerOrderCount(\Magento\Sales\Model\Order $order)
    {
        $customer_id = $order->getCustomerId();
        $_orders =  $this->_orderCollectionFactory->create()->addFieldToFilter('customer_id', $customer_id);
        return $_orders->count();
    }

    protected function createPrefixedAddressArray($address, $prefix)
    {
        return [
            $prefix . 'region' => $address->getRegion(),
            $prefix . 'region_code' => $address->getRegionCode(),
            $prefix . 'street' => $address->getStreet()[0],
            $prefix . 'city' => $address->getCity(),
            $prefix . 'country_id' => $address->getCountryId(),
            $prefix . 'first_name' => $address->getFirstname(),
            $prefix . 'last_name' => $address->getLastname(),
            $prefix . 'postcode' => $address->getPostcode()
        ];
    }

    public function getDiscountArray(\Magento\Sales\Model\Order $order)
    {
        $couponCode = $order->getCouponCode();
        if ($couponCode) {
            $ruleId =  $this->_coupon->loadByCode($couponCode)->getRuleId();
            $rule = $this->_saleRule->load($ruleId);
            return [
                'code' => $couponCode,
                'name' => $rule->getName(),
                'from_date' => $rule->getFromDate(),
                'to_date' => $rule->getToDate(),
                'type' => $rule->getSimpleAction(),
                'is_active' => $rule->getIsActive(),
            ];
        } else {
            return null;
        }
    }

    public function getAddressArray(\Magento\Sales\Model\Order $order)
    {
        return array_merge(
            $this->createPrefixedAddressArray($order->getBillingAddress(), 'billing_address_'),
            $this->createPrefixedAddressArray($order->getShippingAddress(), 'shipping_address_')
        );
    }
}

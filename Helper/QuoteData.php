<?php

namespace Tone\Integration\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\SalesRule\Model\Coupon;
use Magento\SalesRule\Model\Rule;

class QuoteData extends AbstractHelper
{
    public function __construct(
        Coupon $coupon,
        Rule $saleRule
    ) {
        $this->_coupon = $coupon;
        $this->_saleRule = $saleRule;
    }

    public function getBillingAddressPhone(\Magento\Quote\Model\Quote $quote)
    {
        $billingAddress = $quote->getBillingAddress();
        if ($billingAddress) {
            $customerPhone = $billingAddress->getTelephone();
            if (!empty($customerPhone)) return $customerPhone;
        }
        return null;
    }

    public function getShippingAddressPhone(\Magento\Quote\Model\Quote $quote)
    {
        $shippingAddress = $quote->getShippingAddress();
        if ($shippingAddress) {
            $customerPhone = $shippingAddress->getTelephone();
            if (!empty($customerPhone)) return $customerPhone;
        }
        return null;
    }

    public function getCustomerPhone(\Magento\Quote\Model\Quote $quote)
    {
        $billingPhone = $this->getBillingAddressPhone($quote);
        $shippingPhone = $this->getShippingAddressPhone($quote);
        return $billingPhone ? $billingPhone : $shippingPhone;
    }

    public function getCustomerFirstname(\Magento\Quote\Model\Quote $quote)
    {
        $customerFirstname = $quote->getCustomerFirstname();
        if (!empty($customerFirstname)) return $customerFirstname;
        $billingAddress = $quote->getBillingAddress();
        if ($billingAddress) {
            $customerFirstname = $billingAddress->getFirstname();
            if (!empty($customerFirstname)) return $customerFirstname;
        }
        $shippingAddress = $quote->getShippingAddress();
        if ($shippingAddress) {
            $customerFirstname = $shippingAddress->getFirstname();
            if (!empty($customerFirstname)) return $customerFirstname;
        }
        return null;
    }
    public function getCustomerLastname(\Magento\Quote\Model\Quote $quote)
    {
        $customerLastname = $quote->getCustomerLastname();
        if (!empty($customerLastname)) return $customerLastname;
        $billingAddress = $quote->getBillingAddress();
        if ($billingAddress) {
            $customerLastname = $billingAddress->getLastname();
            if (!empty($customerLastname)) return $customerLastname;
        }
        $shippingAddress = $quote->getShippingAddress();
        if ($shippingAddress) {
            $customerLastname = $shippingAddress->getLastname();
            if (!empty($customerLastname)) return $customerLastname;
        }
        return null;
    }
    public function getCustomerEmail(\Magento\Quote\Model\Quote $quote)
    {
        $customerEmail = $quote->getCustomerEmail();
        if (!empty($customerEmail)) return $customerEmail;
        $billingAddress = $quote->getBillingAddress();
        if ($billingAddress) {
            $customerEmail = $billingAddress->getEmail();
            if (!empty($customerEmail)) return $customerEmail;
        }
        $shippingAddress = $quote->getShippingAddress();
        if ($shippingAddress) {
            $customerEmail = $shippingAddress->getEmail();
            if (!empty($customerEmail)) return $customerEmail;
        }
        return null;
    }

    public function getProductsArray(\Magento\Quote\Model\Quote $quote)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceHelper   = $objectManager->create('Magento\Framework\Pricing\Helper\Data');
        $productsArr = [];
        $cartItems = $quote->getAllVisibleItems();
        foreach ($cartItems as $item) {
            $product = [
                'product_id'    => (int) $item->getProductId(),
                'title'         => $item->getName(),
                'sku'           => $item->getSku(),
                'base_price'    => $item->getBasePrice(),
                'price'         => $item->getPrice(),
                'price_display' => $priceHelper->currency($item->getPrice(), true, false),
                'qty'           => $item->getQty()
            ];
            $productsArr[] = $product;
        }
        return $productsArr;
    }

    public function getDiscountArray(\Magento\Quote\Model\Quote $quote)
    {
        $couponCode = $quote->getCouponCode();
        if ($couponCode) {
            $ruleId =  $this->_coupon->loadByCode($couponCode)->getRuleId();
            $rule = $this->_saleRule->load($ruleId);
            return [
                'code' => $couponCode,
                'name' => $rule->getName(),
                'from_date' => $rule->getFromDate(),
                'to_date' => $rule->getToDate(),
                'type' => $rule->getSimpleAction(),
                'is_active' => $rule->getIsActive(),
            ];
        } else {
            return null;
        }
    }

    public function getSMSOptInValue(\Magento\Quote\Model\Quote $quote)
    {
        $shippingAddress = $quote->getShippingAddress();
        return  $shippingAddress->getExtensionAttributes()->getSmsOptIn();
    }

    public function getTotalsArray(\Magento\Quote\Model\Quote $quote)
    {
        $totalsArr = [];
        $totals = $quote->getTotals();
        foreach ($totals as $total) {
            $total = [
                'name'   => (string) $total->getTitle(),
                'amount' => (string) $total->getValue()
            ];
            $totalsArr[] = $total;
        }
        return $totalsArr;
    }
}

<?php

namespace Tone\Integration\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    const GENERAL_CONFIG_PATH = 'Integration/general/';
    const OPT_IN_PATH = 'Integration/optin/';


    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function getGeneralConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::GENERAL_CONFIG_PATH . $code, $storeId);
    }

    public function getOptInConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::OPT_IN_PATH . $code, $storeId);
    }

    public function getOptInConfigLabel($storeId = null)
    {
        return $this->getOptInConfig('label', $storeId);
    }

    public function getOptInConfigDescription($storeId = null)
    {
        return $this->getOptInConfig('description', $storeId);
    }
}

define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, quote) {
    'use strict';

    return function(setShippingInformationAction) {
        return wrapper.wrap(setShippingInformationAction, function(originalAction) {
            var shippingAddress = quote.shippingAddress();
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }
            try {
                $.each(shippingAddress.customAttributes, function(key, value) {
                    if (Number.isInteger(key)) {
                        if ('attribute_code' in value && value.attribute_code === 'sms_opt_in') {
                            shippingAddress['extension_attributes']['sms_opt_in'] = value.value;
                        }
                    } else {
                        if (key === 'sms_opt_in') {
                            if ($.isPlainObject(value)) {
                                value = value['value'];
                            }
                            shippingAddress['extension_attributes']['sms_opt_in'] = value;
                        }
                    }
                });
            } finally {
                return originalAction();
            }
        });
    };
});
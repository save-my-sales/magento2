# Tone Integration for Magento 2

Extension for Magento 2. Enables an integration with the Tone platform.


## Installation

Run the following to install the extension via composer:

    composer require tone/magento2
    php bin/magento module:enable Tone_Integration
    php bin/magento setup:upgrade

## Configure the Integration

Once installed you will need an API Key from you Tone account. If you don't already have an account [go here](https://dashboard.tonemessaging.com/auth/register).

Go to Stores > Settings Configuration > Tone, enter your API key, and click 'Save Config'. You should see a message saying your API Key was valid.

After entering your API key go back to the command line a run the following:

    php bin/magento sms:send:store

## Configure Opt-In

Customers need to opt-in to receiving text messages from Tone. Installing this module will add a checkbox input under the shipping address phone number field. Tone will only send messages to those customers who check that box.

You can edit the label and description for the checkbox Stores > Settings Configuration > Tone > Integration > Opt-In

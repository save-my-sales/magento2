<?php

namespace Tone\Integration\Plugin\Checkout;

use Tone\Integration\Helper\Data;

class LayoutProcessor
{
    protected $dataHelper;

    public function __construct(Data $dataHelper)
    {
        $this->dataHelper = $dataHelper;
    }

    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array $jsLayout
    ) {
        $customAttributeCode = 'sms_opt_in';
        $customField = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'dataScope' => 'shippingAddress.custom_attributes.' . $customAttributeCode,
            'provider' => 'checkoutProvider',
            'sortOrder' => 121,
            'visible' => true,
            'checked' => false,
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'description' => $this->dataHelper->getOptInConfigDescription(),
                'elementTmpl' => 'ui/form/element/checkbox',
                'label' => $this->dataHelper->getOptInConfigLabel(),
                'template' => 'ui/form/field',
            ],
        ];
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'])) {
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$customAttributeCode] = $customField;
        }
        return $jsLayout;
    }
}

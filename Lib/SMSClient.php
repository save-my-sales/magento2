<?php

namespace Tone\Integration\Lib;

use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Tone\Integration\Helper\Data;

class SMSClient
{
    protected $webhooks_api_url;
    protected $dashboard_api_url;
    protected $client;
    protected $logger;

    public function __construct(
        Data $dataHelper,
        LoggerInterface $logger,
        StoreManagerInterface $storeManager
    ) {
        $this->dataHelper = $dataHelper;
        $this->logger = $logger;
        $this->storeManager = $storeManager;
        $this->webhooks_api_url = 'https://webhooks.savemysales.co';
        $this->dashboard_api_url = 'https://dashboard.tonemessaging.com/auth';
    }

    public function verfiyToken($token)
    {
        $token_verify_resp = $this->getClient(\Zend_Http_Client::GET, $token)
            ->setUri($this->dashboard_api_url . '/token/verify')
            ->request();
        return $token_verify_resp->isSuccessful();
    }

    public function get($rel_url, $params)
    {
        return $this->sendRequest(\Zend_Http_Client::GET, $rel_url, $params);
    }

    public function post($rel_url, $body)
    {
        return $this->sendRequest(\Zend_Http_Client::POST, $rel_url, $body);
    }


    public function put($rel_url, $body)
    {
        return $this->sendRequest(\Zend_Http_Client::PUT, $rel_url, $body);
    }


    public function patch($rel_url, $body)
    {
        return $this->sendRequest(\Zend_Http_Client::PATCH, $rel_url, $body);
    }

    public function connectAccountToStore($storeId)
    {
        $connectResp = $this->getClient(\Zend_Http_Client::POST, $this->dataHelper->getConfigValue('Integration/general/api_key'))
            ->setUri($this->dashboard_api_url . '/connectStore/' . $storeId)
            ->request();
        return $connectResp->isSuccessful();
    }

    public function sendRequest($method, $rel_url, $body = [], $params = [])
    {
        $client = $this->getClient($method, $this->dataHelper->getConfigValue('Integration/general/api_key'))
            ->setUri($this->webhooks_api_url . $rel_url);
        if (!empty($body)) {
            $client = $client->setRawData(json_encode($body), 'application/json');
        }
        if (!empty($params)) {
            $client = $client->setParameterGet($params);
        }
        $resp = $client->request();
        if (!$resp->isSuccessful()) {
            $this->logger->debug('SMS Client request to ' . $this->webhooks_api_url . $rel_url . ' failled with status ' . $resp->getStatus() . ': ' . json_encode($resp->getBody()));
        }
        return $resp;
    }

    private function getClient($method, $api_key)
    {
        $client = new \Zend_Http_Client();
        return $client->setMethod($method)
            ->setConfig(['timeout' => 15])
            ->setHeaders('Content-Type', 'application/json')
            ->setHeaders('Store-Type', 'Magento')
            ->setHeaders('Authorization', 'Token ' . $api_key);
    }
}

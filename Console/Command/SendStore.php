<?php

namespace Tone\Integration\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tone\Integration\Model\Store;

class SendStore extends Command
{

    private $store;

    /**
     * @method __construct
     * @param  Store $store
     */
    public function __construct(Store $store)
    {
        $this->store = $store;
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('sms:send:store')
            ->setDescription('Create store in SMS');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $resp = $this->store->sendStore();
        $output->writeln('<info>Store Sent (ID: ' . $resp->id . ')</info>');
    }
}

<?php

namespace Tone\Integration\Model;

use Psr\Log\LoggerInterface;
use Tone\Integration\Helper\Data;
use Tone\Integration\Helper\QuoteData;
use Tone\Integration\Lib\SMSClient;

class Quote
{
    protected $client;
    protected $dataHelper;
    protected $logger;

    public function __construct(
        Data $dataHelper,
        QuoteData $quoteHelper,
        LoggerInterface $logger,
        SMSClient $client
    ) {
        $this->dataHelper = $dataHelper;
        $this->helper = $quoteHelper;
        $this->logger = $logger;
        $this->client = $client;
    }

    public function sendQuote($quote)
    {
        $quoteArr = $this->getQuoteArray($quote);
        $resp =  $this->client->put('/checkouts', $quoteArr);
        return json_decode($resp->getBody());
    }

    protected function getQuoteArray($quote)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceHelper   = $objectManager->create('Magento\Framework\Pricing\Helper\Data');
        $quoteArr = array_merge($quote->toArray(), [
            'sms_store_id'                => $this->dataHelper->getConfigValue('Integration/general/sms_store_id'),
            'first_name'                  => $this->helper->getCustomerFirstname($quote),
            'last_name'                   => $this->helper->getCustomerLastname($quote),
            'email'                       => $this->helper->getCustomerEmail($quote),
            'billing_address_phone'       => $this->helper->getBillingAddressPhone($quote),
            'billing_address_country_id'  => $quote->getShippingAddress()->getCountryId(),
            'shipping_address_phone'      => $this->helper->getShippingAddressPhone($quote),
            'shipping_address_country_id' => $quote->getShippingAddress()->getCountryId(),
            'phone'                       => $this->helper->getCustomerPhone($quote),
            'grand_total'                 => (float) $quote->getBaseGrandTotal(),
            'grand_total_currency'        => $priceHelper->currency($quote->getGrandTotal(), true, false),
            'products'                    => [],
            'discounts'                   => $this->helper->getDiscountArray($quote) ? [$this->helper->getDiscountArray($quote)] : [],
            'totals'                      => []
        ]);
        $quoteArr['sms_opt_in'] = $this->helper->getSMSOptInValue($quote);
        $quoteArr['products'] = $this->helper->getProductsArray($quote);
        $quoteArr['totals'] = $this->helper->getTotalsArray($quote);
        return $quoteArr;
    }
}

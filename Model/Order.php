<?php

namespace Tone\Integration\Model;

use Psr\Log\LoggerInterface;
use Tone\Integration\Lib\SMSClient;
use Tone\Integration\Helper\Data;
use Tone\Integration\Helper\OrderData;

class Order
{
    protected $client;
    protected $dataHelper;
    protected $logger;

    public function __construct(
        Data $dataHelper,
        LoggerInterface $logger,
        OrderData $orderHelper,
        SMSClient $client
    ) {
        $this->client = $client;
        $this->dataHelper = $dataHelper;
        $this->helper = $orderHelper;
        $this->logger = $logger;
    }

    public function sendOrder($order)
    {
        $orderArr = $this->getOrderArray($order);
        $resp =  $this->client->put('/orders', $orderArr);
        return json_decode($resp->getBody());
    }

    protected function getOrderArray($order)
    {
        $address_array = $this->helper->getAddressArray($order);
        $discount_array = $this->helper->getDiscountArray($order);
        $order_count = $this->helper->getCustomerOrderCount($order);
        return array_merge(
            $order->toArray(),
            array_merge(
                $address_array,
                [
                    'sms_store_id' => $this->dataHelper->getConfigValue('Integration/general/sms_store_id'),
                    'customer_order_count' => $order_count,
                    'source_name' => 'web',
                    'discounts' => $discount_array ? [$discount_array] : []
                ]
            )
        );
    }
}

<?php

namespace Tone\Integration\Model;

use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\App\Cache\Type\Config as CacheTypeConfig;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\UrlInterface;
use Magento\Sales\Model\Order\Config as OrderConfig;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Tone\Integration\Helper\Data;
use Tone\Integration\Lib\SMSClient;


class Store
{
    protected $client;
    protected $dataHelper;
    protected $logger;

    public function __construct(
        Data $dataHelper,
        LoggerInterface $logger,
        OrderConfig $orderConfig,
        ScopeConfigInterface $scopeConfigInterface,
        StoreManagerInterface $storeManager,
        SMSClient $client,
        UrlInterface $urlInterface,
        CacheManager $cacheManager,
        WriterInterface $configWriter
    ) {
        $this->dataHelper = $dataHelper;
        $this->logger = $logger;
        $this->storeManager = $storeManager;
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->orderConfig = $orderConfig;
        $this->urlInterface = $urlInterface;
        $this->client = $client;
        $this->configWriter = $configWriter;
        $this->cacheManager = $cacheManager;
    }

    public function sendStore()
    {
        $sms_store_id = $this->dataHelper->getConfigValue('Integration/general/sms_store_id');
        $full_store = $this->getStoreArray($this->storeManager->getStore(), $sms_store_id);
        if ($sms_store_id) {
            $resp =  $this->client->put('/shop', $full_store);
        } else {
            $resp =  $this->client->put('/shop', $full_store);
            $sms_store_id = json_decode($resp->getBody())->id;
            $this->configWriter->save('Integration/general/sms_store_id', $sms_store_id);
            $this->cacheManager->clean([CacheTypeConfig::TYPE_IDENTIFIER]);
        }
        $this->client->connectAccountToStore($sms_store_id);
        return json_decode($resp->getBody());
    }

    private function getStoreArray($store, $sms_store_id = null)
    {
        $store_arr =  array_merge($store->toArray(), [
            'url'                    => $store->getBaseUrl(UrlInterface::URL_TYPE_WEB),
            'config_name'            => $this->scopeConfigInterface->getValue('general/store_information/name'),
            'customer_support_email' => $this->scopeConfigInterface->getValue('trans_email/ident_support/email'),
            'timezone'               => $this->scopeConfigInterface->getValue('general/locale/timezone'),
            'phone'                  => $this->scopeConfigInterface->getValue('general/store_information/phone'),
            'hours'                  => $this->scopeConfigInterface->getValue('general/store_information/hours'),
            'address1'               => $this->scopeConfigInterface->getValue('general/store_information/street_line1'),
            'address2'               => $this->scopeConfigInterface->getValue('general/store_information/street_line2'),
            'city'                   => $this->scopeConfigInterface->getValue('general/store_information/city'),
            'zip'                    => $this->scopeConfigInterface->getValue('general/store_information/postcode'),
            'region_id'              => $this->scopeConfigInterface->getValue('general/store_information/region_id'),
            'country_code'           => $this->scopeConfigInterface->getValue('general/store_information/country_id'),
            'merchant_vat_number'    => $this->scopeConfigInterface->getValue('general/store_information/merchant_vat_number'),
            'enabled'                => $this->scopeConfigInterface->getValue('general/single_store_mode/enabled'),
        ]);
        if ($sms_store_id) {
            $store_arr['sms_id'] = $sms_store_id;
        }
        return $store_arr;
    }
}

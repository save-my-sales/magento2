<?php

namespace Tone\Integration\Observer;

use \Magento\Framework\Event\ObserverInterface;
use \Tone\Integration\Model\Order;
use \Psr\Log\LoggerInterface;

class OrderObserver implements ObserverInterface
{
    protected $logger;
    protected $order;

    public function __construct(
        Order $order,
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
        $this->order = $order;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        if (!$order->getId()) {
            $this->logger->info("Invalid Order");
        } else {
            $this->order->sendOrder($order);
        }
    }
}

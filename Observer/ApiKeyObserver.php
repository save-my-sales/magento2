<?php

namespace Tone\Integration\Observer;

use Psr\Log\LoggerInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Tone\Integration\Lib\SMSClient;

class ApiKeyObserver implements ObserverInterface
{
    protected $messageManager;
    protected $dataHelper;
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Tone\Integration\Helper\Data $dataHelper,
        LoggerInterface $logger,
        SMSClient $client
    ) {
        $this->messageManager = $messageManager;
        $this->dataHelper = $dataHelper;
        $this->client = $client;
        $this->logger = $logger;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $field = $observer->getEvent()['config_data']->getData();
        if (!array_key_exists('field', $field) || $field['field'] !== 'api_key') return;
        $api_key = $field['value'];
        if (!$api_key) return;
        $tokenVerified = $this->client->verfiyToken($api_key);
        if ($tokenVerified) {
            $this->messageManager->addSuccessMessage('API Key added successfully');
        } else {
            $this->messageManager->addErrorMessage('API Key is not valid');
        }
    }
}

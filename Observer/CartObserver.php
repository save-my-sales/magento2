<?php

namespace Tone\Integration\Observer;

use Magento\Framework\Event\ObserverInterface;
use \Psr\Log\LoggerInterface;
use \Tone\Integration\Model\Quote;

class CartObserver implements ObserverInterface
{
    protected $logger;
    protected $quote;

    public function __construct(
        Quote $quote,
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
        $this->quote = $quote;
    }

    /*
     * A checkout is considered abandoned when a customer has entered billing 
     * and shipping information, but hasn't completed the purchase
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $event = $observer->getEvent();
        if ($event->getCart()) {
            $quote = $event->getCart()->getQuote();
        } else {
            $quote = $event->getQuote();
        }

        if (!$quote->getId()) {
            $this->logger->info("Invalid Cart");
        } else {
            $this->quote->sendQuote($quote);
        }
    }
}
